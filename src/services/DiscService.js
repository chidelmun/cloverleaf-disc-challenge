import data from '../data/mapping-data';

/**
 * Evaluate DISC assessment answers
 * @param  {array} answers Array of answers in the form: `{ description: STRING, rank: NUMBER }`
 * @return {object}        final DISC score
 *
 * Return object format
 * {
 *   d: <Number>,
 *   i: <Number>,
 *   s: <Number>,
 *   c: <Number>,
 * }
 */
export function evaluate(answers) {
  // TODO: Implement this function to get the user's DISC score.
}

export default { evaluate };
