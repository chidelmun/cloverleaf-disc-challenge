module.exports = {
  persuasive: {
    letter: 'i',
    m: 1,
    l: 0,
  },
  gentle: {
    letter: 's',
    m: 0,
    l: 1,
  },
  humble: {
    letter: 'c',
    m: 1,
    l: 0,
  },
  original: {
    letter: 'd',
    m: 1,
    l: 0,
  },

  aggressive: {
    letter: 'd',
    m: 0,
    l: 1,
  },
  'life-of-the-party': {
    letter: 'i',
    m: 0,
    l: 1,
  },
  'easy mark': {
    letter: 's',
    m: 1,
    l: 1,
  },
  fearful: {
    letter: 'c',
    m: 1,
    l: 1,
  },

  sweet: {
    letter: 's',
    m: 0,
    l: 0,
  },
  cooperative: {
    letter: 'c',
    m: 0,
    l: 1,
  },
  tenacious: {
    letter: 'd',
    m: 1,
    l: 0,
  },
  attractive: {
    letter: 'i',
    m: 1,
    l: 1,
  },

  cautious: {
    letter: 'c',
    m: 1,
    l: 0,
  },
  determined: {
    letter: 'd',
    m: 0,
    l: 1,
  },
  convincing: {
    letter: 'i',
    m: 0,
    l: 1,
  },
  'good-natured': {
    letter: 's',
    m: 1,
    l: 1,
  },

  docile: {
    letter: 'c',
    m: 1,
    l: 1,
  },
  bold: {
    letter: 'd',
    m: 0,
    l: 1,
  },
  loyal: {
    letter: 's',
    m: 1,
    l: 0,
  },
  charming: {
    letter: 'i',
    m: 1,
    l: 0,
  },

  willing: {
    letter: 's',
    m: 1,
    l: 0,
  },
  poised: {
    letter: 'd',
    m: 0,
    l: 0,
  },
  agreeable: {
    letter: 'c',
    m: 0,
    l: 1,
  },
  'high-spirited': {
    letter: 'd',
    m: 1,
    l: 1,
  },

  'will power': {
    letter: 'd',
    m: 1,
    l: 0,
  },
  'open-minded': {
    letter: 'c',
    m: 1,
    l: 0,
  },
  obliging: {
    letter: 's',
    m: 1,
    l: 1,
  },
  cheerful: {
    letter: 'i',
    m: 1,
    l: 0,
  },

  confident: {
    letter: 'i',
    m: 0,
    l: 1,
  },
  sympathetic: {
    letter: 's',
    m: 1,
    l: 0,
  },
  tolerant: {
    letter: 'c',
    m: 0,
    l: 1,
  },
  assertive: {
    letter: 'd',
    m: 1,
    l: 0,
  },

  'even-tempered': {
    letter: 's',
    m: 0,
    l: 1,
  },
  precise: {
    letter: 'c',
    m: 1,
    l: 1,
  },
  nervy: {
    letter: 'd',
    m: 0,
    l: 1,
  },
  jovial: {
    letter: 'i',
    m: 1,
    l: 1,
  },

  'well-disciplined': {
    letter: 'c',
    m: 1,
    l: 1,
  },
  generous: {
    letter: 's',
    m: 1,
    l: 1,
  },
  animated: {
    letter: 'i',
    m: 1,
    l: 0,
  },
  persistent: {
    letter: 'd',
    m: 1,
    l: 0,
  },

  competitive: {
    letter: 'd',
    m: 1,
    l: 0,
  },
  outgoing: {
    letter: 'i',
    m: 1,
    l: 0,
  },
  considerate: {
    letter: 's',
    m: 0,
    l: 1,
  },
  harmonious: {
    letter: 'c',
    m: 1,
    l: 1,
  },

  admirable: {
    letter: 'i',
    m: 1,
    l: 1,
  },
  kind: {
    letter: 's',
    m: 1,
    l: 0,
  },
  resigned: {
    letter: 'c',
    m: 0,
    l: 1,
  },
  'force of character': {
    letter: 'd',
    m: 1,
    l: 1,
  },

  obedient: {
    letter: 's',
    m: 1,
    l: 1,
  },
  fussy: {
    letter: 'c',
    m: 1,
    l: 1,
  },
  unconquerable: {
    letter: 'd',
    m: 0,
    l: 1,
  },
  playful: {
    letter: 'i',
    m: 0,
    l: 1,
  },

  respectful: {
    letter: 'c',
    m: 0,
    l: 1,
  },
  pioneering: {
    letter: 'd',
    m: 1,
    l: 0,
  },
  optimistic: {
    letter: 'i',
    m: 1,
    l: 1,
  },
  accommodating: {
    letter: 's',
    m: 0,
    l: 1,
  },

  brave: {
    letter: 'd',
    m: 0,
    l: 1,
  },
  inspiring: {
    letter: 'i',
    m: 1,
    l: 1,
  },
  submissive: {
    letter: 's',
    m: 1,
    l: 1,
  },
  timid: {
    letter: 'c',
    m: 1,
    l: 0,
  },

  adaptable: {
    letter: 'c',
    m: 1,
    l: 0,
  },
  argumentative: {
    letter: 'd',
    m: 1,
    l: 1,
  },
  nonchalant: {
    letter: 's',
    m: 0,
    l: 1,
  },
  'light-hearted': {
    letter: 'i',
    m: 1,
    l: 1,
  },

  gregarious: {
    letter: 'i',
    m: 1,
    l: 1,
  },
  patient: {
    letter: 's',
    m: 0,
    l: 1,
  },
  'self-reliant': {
    letter: 'd',
    m: 1,
    l: 0,
  },
  'soft-spoken': {
    letter: 'c',
    m: 1,
    l: 1,
  },

  contented: {
    letter: 's',
    m: 0,
    l: 1,
  },
  trusting: {
    letter: 'i',
    m: 0,
    l: 0,
  },
  peaceful: {
    letter: 'c',
    m: 1,
    l: 1,
  },
  positive: {
    letter: 'd',
    m: 1,
    l: 1,
  },

  adventurous: {
    letter: 'd',
    m: 1,
    l: 0,
  },
  receptive: {
    letter: 'c',
    m: 1,
    l: 1,
  },
  cordial: {
    letter: 'i',
    m: 1,
    l: 1,
  },
  moderate: {
    letter: 's',
    m: 0,
    l: 1,
  },

  lenient: {
    letter: 's',
    m: 1,
    l: 1,
  },
  aesthetic: {
    letter: 'c',
    m: 0,
    l: 1,
  },
  vigorous: {
    letter: 'd',
    m: 1,
    l: 1,
  },
  'good-mixer': {
    letter: 'i',
    m: 1,
    l: 1,
  },

  talkative: {
    letter: 'i',
    m: 1,
    l: 1,
  },
  controlled: {
    letter: 's',
    m: 0,
    l: 1,
  },
  conventional: {
    letter: 'c',
    m: 1,
    l: 0,
  },
  decisive: {
    letter: 'd',
    m: 1,
    l: 0,
  },

  restrained: {
    letter: 's',
    m: 1,
    l: 0,
  },
  accurate: {
    letter: 'c',
    m: 1,
    l: 1,
  },
  outspoken: {
    letter: 'd',
    m: 0,
    l: 1,
  },
  companionable: {
    letter: 'i',
    m: 1,
    l: 0,
  },

  diplomatic: {
    letter: 'c',
    m: 1,
    l: 1,
  },
  audacious: {
    letter: 'd',
    m: 0,
    l: 1,
  },
  polished: {
    letter: 'i',
    m: 1,
    l: 0,
  },
  satisfied: {
    letter: 's',
    m: 0,
    l: 1,
  },

  restless: {
    letter: 'd',
    m: 0,
    l: 1,
  },
  popular: {
    letter: 'i',
    m: 1,
    l: 0,
  },
  neighborly: {
    letter: 's',
    m: 1,
    l: 0,
  },
  orderly: {
    letter: 'c',
    m: 0,
    l: 1,
  },
};
