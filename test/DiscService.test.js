/* globals describe it */
import { expect } from 'chai';
import { evaluate } from '../src/services/DiscService';
import answers from './DISC-test-answers';

describe('DISC Evalution', () => {
  const emptyDisc = {
    d: 0,
    i: 0,
    s: 0,
    c: 0,
  };

  it('should evaluate empty scores', () => {
    const disc = evaluate([]);
    expect(disc).to.eql(emptyDisc);
  });

  it('should evaluate scores', () => {
    const disc = evaluate(answers);
    const expected = {
      d: 3,
      i: 5,
      s: -2,
      c: -2,
    };
    expect(disc).to.eql(expected);
  });

  it('should not map for ranks 2 and 3', () => {
    const ans = [{ description: 'trusting', rank: 3 }, { description: 'positive', rank: 2 }];
    const disc = evaluate(ans);
    expect(disc).to.eql(emptyDisc);
  });
});
