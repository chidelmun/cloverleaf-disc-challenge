# cloverleaf-disc-challenge

Welcome to the Cloverleaf coding challenge!

Your challenge is to implement the function(s) to calculate the score for a personality assessment.

### Requirements
Node.js 9.10

### Instructions
1. Install Dependencies
```npm install```

2. Run Tests
```npm test``` or ```npm run test:watch```

3. Implement the `evaluate(answers)` function in `src/services/DiscService.js` so that all tests pass.


Answers Data Format (as seen in `test/DISC-test-answers.js`):
```
[
  {
    description: <String>,
    rank: <Number>,
  },
]
```

Response Format:
```
{
  d: <Number>,
  i: <Number>,
  s: <Number>,
  c: <Number>,
}
```

### About DISC
DISC is an assessment that analyzes your personality based on four traits - Dominance, Influence, Steadiness, and Compliance.
Imagine you are given a list of descriptive words that you must rank with the numbers 1-4 indicating how much the word is like you.
Your answers are then mapped to which DISC letter they correspond to.

In this case, we're using a made up, simplified version of the DISC mapping data.

Keep in mind:

Words ranked 1 are considered "motivated".

Words ranked 4 are considered "latent".

Anything ranked 2 or 3 is ignored.

Your function should compare the user's answers to the data in `src/data/mapping-data.js`. The mapping data will tell you which letter (d, i, s, or c) the word relates to, and it's "motivated" or "latent" value.

### Example:

Answers given are:
```
[
  { description: 'persuasive', rank: 1 },
  { description: 'gentle', rank: 2 },
  { description: 'humble', rank: 4 },
]
```
From the mapping data:
```
{
  persuasive: {
    letter: 'i',
    m: 1,
    l: 0,
  },
  humble: {
    letter: 'c',
    m: 1,
    l: 0,
  }
}
```
"persuasive" was ranked 1, which means "motivated", so we will increment the "motivated" value for "I" by the value in the mapping - which is 1.

"gentle" was ranked 2, so we can ignore it.

"humble" was ranked 4, which means "latent", so we will increment the "latent" value for "C" by the value in the mapping - which is 0.

At this point, our DISC score is:
```
{
  d: { motivated: 0, latent: 0 },
  i: { motivated: 1, latent: 0 },
  s: { motivated: 0, latent: 0 },
  c: { motivated: 0, latent: 0 },
}
```

At the end, subtract the latent value from the motivated value of each letter to get the final DISC score:
```
{
  d: 0,
  i: 1,
  s: 0,
  c: 0,
}
```

### Next Steps

Send your completed solution to `matt@cloverleaf.me`


